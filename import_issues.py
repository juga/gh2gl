"""Quick and dirty script to import issues from a exported gl project to
an existing one, assuming there aren't conflicts with issues numbers."""

import json
import gitlab


# CHANGEME!
TARGET_PROJECT_ID = 596
gl = gitlab.Gitlab.from_config('gitlab', ['/path/to/.python-gitlab.cfg'])

target_project = gl.projects.get(TARGET_PROJECT_ID)

# GH issues can be imported as they are, since there is not conflict in numbers
with open(ISSUES_PATH) as fd:
    issues_lines = fd.readlines()


for issue_line in issues_lines:
    issue_dict = json.loads(issue_line)
    try:
        issue = target_project.issues.create(issue_dict)
        print(issue)
    except Exception as e:
        print(e)
        issue = target_project.issues.get(issue_dict["iid"])
    print(issue)
    # Change the state to what corresponds
    if issue_dict["state"] == "closed":
        issue.state_event = 'close'
        issue.save()
        print("closed")
    # If importing a second time,
    # notes get duplicated cause don't have iid, so try to delete all
    # existing ones first.
    for note in issue.notes.list(all=True):
        print(note)
        try:
            note.delete()
        except gitlab.exceptions.GitlabDeleteError as e:
            print(e)
    # And then add them.
    for note_dict in issue_dict["notes"]:
        note_dict['body'] = note_dict['note']
        try:
            note = issue.notes.create(note_dict)
        except Exception as e:
            print(e)
            note = issue.notes.get(note_dict["id"])
    print(note)
